# proveedores_app/serializers.py
from rest_framework import serializers
from .models import Proveedores

class ProveedorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proveedores
        fields = '__all__'