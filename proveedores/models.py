from django.db import models

# Create your models here.

from django.db import models

class Proveedores(models.Model):
    nombre = models.CharField(max_length=100)
    nif = models.CharField(max_length=20)
    direccion = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre