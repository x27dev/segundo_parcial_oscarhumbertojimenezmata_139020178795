# proveedores_app/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('proveedores/', views.proveedores, name='proveedores'),
    path('crear_proveedores/', views.crear_proveedores, name='crear_proveedores'),
    path('editar_proveedores/<int:proveedor_id>/', views.editar_proveedores, name='editar_proveedores'),
]
