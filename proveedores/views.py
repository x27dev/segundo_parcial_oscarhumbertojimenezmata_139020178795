from django.shortcuts import render, redirect, get_object_or_404
from rest_framework import generics

from .forms import ProveedoresForm
from .serializers import ProveedorSerializer
# Create your views here.


from rest_framework import generics
from proveedores.models import Proveedores

def proveedores(request):
  proveedores = Proveedores.objects.all()

  return render(request, 'proveedores/proveedores.html',
                {'proveedores': proveedores})

def crear_proveedores(request):
    if request.method == 'POST':
        form = ProveedoresForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('proveedores')
    else:
        form = ProveedoresForm()
    return render(request, 'proveedores/crear_proveedores.html', {'form': form})

def editar_proveedores(request, proveedor_id):
    proveedores = get_object_or_404(Proveedores, pk=proveedor_id)

    if request.method == 'POST':
        form = ProveedoresForm(request.POST, instance=proveedores)
        if form.is_valid():
            form.save()
            return redirect('proveedores')  # Redirige a la lista de países después de editar
    else:
        form = ProveedoresForm(instance=proveedores)

    return render(request, 'proveedores/editar_proveedores.html', {'form': form, 'proveedores': proveedores()})


