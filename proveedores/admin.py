from django.contrib import admin

from .models import Proveedores

# Register your models here.

class ProveedoresAdmin(admin.ModelAdmin):
  list_display = ('id', 'nombre', 'nif', 'direccion')
  search_fields = ('nombre','nif')
  list_editable = ('nombre', 'nif', 'direccion')

admin.site.register(Proveedores, ProveedoresAdmin)
